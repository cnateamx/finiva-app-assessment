import os
from flask import Flask, render_template

app = Flask(__name__)

bgcolor = os.environ.get('APP_COLOR') or "green"
demo_greeting = os.environ.get('demo_greeting') or "Hello"

@app.route("/")
def home():
    return render_template('index.html', bgcolor=bgcolor, demo_greeting=demo_greeting)

if __name__ == '__main__':
    app.run(port=8080, host="0.0.0.0")
