# Base image used
FROM python:3.9-slim-buster

# Set /app as the working directory
# <Insert Answer Here>

# Copy requirements.txt file from local directory into the root directory of the Docker image
# <Insert Answer Here>

# Run the command 'pip install -r requirements.txt'
# This tells Docker to install Python packages into the Docker image during build
# <Insert Answer Here>

# Copy current app folder into container
# <Insert Answer Here>

# Command to start application once the container is ready
CMD ["python", "./app/app.py"]
